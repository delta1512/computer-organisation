# The main program is to perform calculation as per formula: (s1 + s2) - (s3 - s4)
#
# places variables 12, -2, 13, 3
# in registers s1, s2, s3, s4

  .data
  .globl  message
message:  .asciiz "The value of f is: "      # Store a string variable called "message" for later use
extra:    .asciiz "\nHave a nice day :)"     # Store a string variable called "extra" for later use
thankyou: .asciiz "\n\n\n ... Thank you :)"  # Store a string variable called "thankyou" for later use 
  .align 2                  # align directive will be explained later

  .text
  .globl main
main:                       # Main has to be a global label
  addu  $s7, $0, $ra        # Save return address to a register

        # CALCULATING

  addi  $s1, $0, 12         # Prepares $s1 with value 12 for calculation
  addi  $s2, $0, -2         # Prepares $s2 with value -2 for calculation
  addi  $s3, $0, 13         # Prepares $s3 with value 13 for calculation
  addi  $s4, $0, 3          # Prepares $s4 with value 3 for calculation
  add   $t0, $s1, $s2       # Adds $s1 + $s2 to calculate brackets (12 + (-2))
                            # this is stored in tmp register 0
  sub   $t1, $s3, $s4       # Subtracts $s3 - $s4 to calculate brackets (13 - 3)
                            # this is stored in tmp register 1
  sub   $s0, $t0, $t1       # Subtracts and stores the result of the two brackets into $s0
                            # (12 - 2) - (13 - 3)

        # PRINTING

        # Print the string "The value of f is: "
  li    $v0, 4              # Loads the print_str syscall code into $v0
  la    $a0, message        # Stores the pointer to the desired string in $a0
                            # this is an argument to syscall so it can be printed
  syscall

        # Print the result from the calculation above
  li    $v0, 1              # Load the syscall code for print_int so we can print the result
  add   $a0, $0, $s0        # Stores the resulting integer to $a0
                            # this is an argument to syscall so it can be printed
  syscall

        # Print the "Have a nice day" string
  li    $v0, 4              # Loads the print_str syscall code into $v0
  la    $a0, extra          # Stores the pointer to the desired string in $a0
                            # this is an argument to syscall so it can be printed
  syscall

  la    $a0, thankyou       # Stores the pointer to the desired string in $a0
                            # this is an argument to syscall so it can be printed
  syscall

        # Usual stuff at the end of the main

  addu  $ra, $0, $s7        # restore the return address
  jr    $ra                 # return to the main program
  add   $0, $0, $0          # nop (no operation)
