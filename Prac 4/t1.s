.text

main:
	move $s7, $ra		# Save return address
	
	li $t0, 12		# Load the index of 3
	li $t1, 20		# Load the index of 5
	
	lw $t2, Z($t0)		# Get Z[3]
	lw $t3, Z($t1)		# Get z[5]
	
	sub $s0, $t2, $t3	# X = z[3] - Z[5]
	
	sw $s0, X		# Store X into memory
	
	li $v0, 1		# Load the print_int syscall
	move $a0, $s0		# Load X as an argument to syscall
	syscall
	
	
	# Return
	move $ra, $s7
	jr $ra
	nop

.data
	Z: .word 7, 3, 8, 10, 2, 4, 14, 0, 9, 99
	X: .word 0
	.globl main
