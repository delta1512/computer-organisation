# Used registers:
#
# $t0 stores k
# $t1 stores k+m
# $t2 stores Z[k]
# $t3 stores Z[k+m]
# $t4 stores 12 * 4
# $s0 stores Z[k] + Z[k+m]
# $s7 stores return address

.text
main:
	move $s7, $ra			# Store the return address
	
	
	li $v0, 4			# Load print_str syscall
	la $a0, promptk			# Load the prompt for k
	syscall
	
	li $v0, 5			# Load print_int syscall
	syscall
	
	move $t0, $v0			# save k
	
	li $v0, 4			# Load print_str syscall
	la $a0, promptm			# Load the prompt for m
	syscall
	
	li $v0, 5			# Load print_int syscall
	syscall
	
	add $t1, $v0, $t0		# save k + m
	
	# Convert to relative addresses (multiply by 4)
	mul $t0, $t0, 4
	mul $t1, $t1, 4
	
	lw $t2, Z($t0)			# Load Z[k]
	lw $t3, Z($t1)			# Load Z[k+m]
	
	add $s0, $t2, $t3		# Calc Z[k] + Z[k+m]
	
	li $t4, 48			# load 12 * 4 as the index of 12
	sw $s0, Z($t4)			# Store Z[12] = Z[k] + Z[k+m]
	
	li $v0, 4			# Load the print_str syscall
	la $a0, answer			# Load the prompt for the answer
	syscall
	
	li $v0, 1			# Load the print_int syscall
	move $a0, $s0			# Load Z[12] as an argument
	syscall
	
	
	# Return
	move $ra, $s7
	jr $ra
	nop


.data
	.align 2
	Z: .word 4, 10,	25, 8, 17, 8, 7, 12, 5, 19, 25, 14, 99
	promptk: .asciiz "Type value for k: "
	promptm: .asciiz "\nType value for m: "
	answer: .asciiz "\nThe value of Z[k] + Z[k+m] is: " 
	.globl main
