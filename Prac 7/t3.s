.globl main

.text
main:
	li $v0, 4		# Load print_str
	la $a0, promptp		# Load the prompt for value p
	syscall			# print

	li $v0, 5		# Load read_int
	syscall

	beq $v0, $0, exit	# If 0, exit
	move $t4, $v0		# Temporarily store p

	li $v0, 4		# Load print_str
	la $a0, promptn		# Load the prompt for value n
	syscall			# print

	li $v0, 5		# Load read_int
	syscall
	
	move $s0, $v0		# Save n for later
	move $a1, $v0		# Load n as an argument

	li $v0, 4		# Load print_str
	la $a0, promptm		# Load the prompt for value m
	syscall			# print

	li $v0, 5		# Load read_int
	syscall

	move $a2, $v0		# Save m as an argument

	# Input sanitisation
	add $t0, $s0, $a2	# m + n
	li $t1, 32		# load variable 32
	bgt $t0, $t1, exitbadmn	# If m+n > 32, then exit program
	ble $s0, $0, exitbadmn	# If n <= 0, then exit program
	blt $a2, $0, exitbadmn	# If m < 0, then exit program

	move $a0, $t4		# Load p as an argument


	# Prepare for function call
	subi $sp, $sp, 4	# Make room for 1 word
	sw $ra, ($sp)		# Push return address

	# Call the extract function
	jal extract

	lw $ra, ($sp)		# Pop return address
	addi $sp, $sp, 4	# Reset the stack pointer


	li $t1, 32		# Load 32
	sub $t1, $t1, $s0 	# 32 - n

	# Manage signed value
	sllv $t0, $v0, $t1	# Shift resulting MSB to highest position in register
	srav $s0, $t0, $t1	# Shift back while preserving sign

	# Manage unsigned value
	move $s1, $v0		# No need to shift for unsigned value


	li $v0, 4		# Load print_str
	la $a0, promptsign	# Load the prompt for the signed value
	syscall			# print

	move $a0, $s0		# Load the signed value as argument
	li $v0, 1		# Load print_int
	syscall			# print

	li $v0, 4		# Load print_str
	la $a0, promptusign	# Load the prompt for the unsigned value
	syscall			# print

	move $a0, $s1		# Load the unsigned value as argument
	li $v0, 1		# Load print_int
	syscall			# print

	b main			# Loop until p = 0

exit:
	li $v0, 4		# Load print_str
	la $a0, term		# Load terminating message
	syscall			# print
	# Return
	jr $ra
	nop

exitbadmn:
	li $v0, 4		# Load print_str
	la $a0, badmn		# Load error message
	syscall			# print
	b exit			# Finish exiting properly


# a0: p (number to operate on)
# a1: n (size of mask)
# a2: m (bit index starting from 1)
extract:
	subi $sp, $sp, 4	# Make room for 1 word
	sw $s0, ($sp)		# Push s0 to stack

	# Mask creation process:
	# 1. set MSB as 1
	# 2. srav for n times
	# 3. rol for n+m times
	add $t0, $a1, $a2	# Calc n+m
	subi $t1, $a1, 1	# Calc n-1
	li $s0, 1		# load 1 into the mask register
	ror $s0, $s0, 1		# Set MSB as 1
	srav $s0, $s0, $t1	# Create n-1 bits
	rol $s0, $s0, $t0	# Put mask into position

	and $t2, $s0, $a0	# Extract the value from p
	srlv $v0, $t2, $a2	# Shift the extracted value to LSB and set up output


	lw $s0, ($sp)		# Pop s0 from stack
	addi $sp, $sp, 4	# Reset the stack pointer

	jr $ra			# Return to the caller


.data
	promptp: .asciiz "\nInput an integer: "
	promptn: .asciiz "Input a value for n: "
	promptm: .asciiz "Input a value for m: "
	promptsign: .asciiz "Signed output: "
	promptusign: .asciiz "\nUnsigned output: "
	badmn: .asciiz "\nPlease enter valid m and n values."
	term: .asciiz "\nProgram is terminating..."
