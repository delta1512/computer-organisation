.globl main

.text
main:
	li $v0, 4		# Load print_str
	la $a0, promptp		# Load the prompt for value p
	syscall			# print

	li $v0, 5		# Load read_int
	syscall

	beq $v0, $0, exit	# If 0, exit
	move $a0, $v0		# Load p as an argument


	# Prepare for function call
	subi $sp, $sp, 4	# Make room for 1 word
	sw $ra, ($sp)		# Push return address

	# Call the extract function
	jal extract

	lw $ra, ($sp)		# Pop return address
	addi $sp, $sp, 4	# Reset the stack pointer


	# Manage signed value
	sll $t0, $v0, 27	# Shift resulting MSB to highest position in register
	sra $s0, $t0, 27	# Shift back while preserving sign

	# Manage unsigned value
	move $s1, $v0		# No need to shift for unsigned value


	li $v0, 4		# Load print_str
	la $a0, promptsign	# Load the prompt for the signed value
	syscall			# print

	move $a0, $s0		# Load the signed value as argument
	li $v0, 1		# Load print_int
	syscall			# print

	li $v0, 4		# Load print_str
	la $a0, promptusign	# Load the prompt for the unsigned value
	syscall			# print

	move $a0, $s1		# Load the unsigned value as argument
	li $v0, 1		# Load print_int
	syscall			# print

	b main			# Loop until p = 0

exit:
	# Return
	jr $ra
	nop


extract:
	li $t0, 0x1f		# Load a 5 bit mask
	sll $t0, $t0, 3		# Shift it to the left 3 spaces
	and $t1, $a0, $t0	# Extract the value needed using a mask
	srl $v0, $t1, 3		# Shift the number back to the LSB
	jr $ra			# Return to the caller


.data
	promptp: .asciiz "\nInput an integer: "
	promptsign: .asciiz "Signed output: "
	promptusign: .asciiz "\nUnsigned output: "
