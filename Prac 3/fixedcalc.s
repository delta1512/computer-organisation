# The main program is to perform calculation as per formula: f = (g + h) - (i - j + k)
#
# places variables 12, -2, 13, 3, 1
# in registers s1, s2, s3, s4, s5

  .data
  .globl  message
message:  .asciiz "The value of f is: "      # Store a string variable called "message" for later use
extra:    .asciiz "\nHave a nice day :)"     # Store a string variable called "extra" for later use
thankyou: .asciiz "\n\n\n ... Thank you :)"  # Store a string variable called "thankyou" for later use 
  .align 2                  # align directive will be explained later

  .text
  .globl main
main:                       # Main has to be a global label
  addu  $s7, $0, $ra        # Save return address to a register

        # CALCULATING

  addi  $s1, $0, 12         # let g = 12
  addi  $s2, $0, -2         # let h = -2
  addi  $s3, $0, 13         # let i = 13
  addi  $s4, $0, 3          # let j = 3
  addi  $s5, $0, 1	    # let k = 1
  add   $t0, $s1, $s2       # let x = (a + b)
  sub   $t1, $s3, $s4       # let y = (i - j)
  add   $t1, $t1, $s5	    # y = y + k
  sub   $s0, $t0, $t1       # f = (g + h) - (i - j + k) = (12 - 2) - (13 - 3 + 1)

        # PRINTING

        # Print the string "The value of f is: "
  li    $v0, 4              # Loads the print_str syscall code into $v0
  la    $a0, message        # Stores the pointer to the desired string in $a0
                            # this is an argument to syscall so it can be printed
  syscall

        # Print the result from the calculation above
  li    $v0, 1              # Load the syscall code for print_int so we can print the result
  add   $a0, $0, $s0        # Stores the resulting integer to $a0
                            # this is an argument to syscall so it can be printed
  syscall

        # Print the "Have a nice day" string
  li    $v0, 4              # Loads the print_str syscall code into $v0
  la    $a0, extra          # Stores the pointer to the desired string in $a0
                            # this is an argument to syscall so it can be printed
  syscall

  la    $a0, thankyou       # Stores the pointer to the desired string in $a0
                            # this is an argument to syscall so it can be printed
  syscall

        # Usual stuff at the end of the main

  addu  $ra, $0, $s7        # restore the return address
  jr    $ra                 # return to the main program
  add   $0, $0, $0          # nop (no operation)
