# this programs asks for temperature in Fahrenheit
# and converts it to Celsius

  .text    
         .globl main  
main:                 # main has to be a global label
  addu  $s7, $0, $ra  # save return address in a global register
  
  
      # GETTING INPUT VALUE
  
  la $a0,input        # print message "input" on the console
  li $v0,4
  syscall

  li $v0, 5	      # Load the read_int syscall
  syscall	      # Read integer from user
  
      # CALCULATING  
      # let F = user input
      # t is the final result
      
  addi $t0,$v0,-32    # t = F - 32
  mul  $t0,$t0,5      # t = t * 5
  div  $t0,$t0,9      # t = t / 9 (integer division, no decimals)

      # PRINTING

  la $a0,result       # Load the address of the result prompt to be printed
  li $v0,4            # Load the print_string syscall code
  syscall             # Print the string

  move $a0,$t0        # Get the result (t) ready for printing
  li $v0,1            # Load the print_int syscall
  syscall             # Print the resulting temperature in Celsius

  .data
input:  .asciiz "\n\nEnter temperature in Fahrenheit: "
result:  .asciiz "The temperature in Celsius is: "
  
  .text
  addu  $ra, $0, $s7 # restore the return address
  jr  $ra            # return to the main program
  add  $0, $0, $0    # nop (no operation)