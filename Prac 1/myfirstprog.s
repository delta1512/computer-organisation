# 19185398 Marcus Belcastro
# Modified helloimproved.s

.text
  .globl main
  main:                    # The global main function
    addu $s7, $0, $ra      # Saves the return address in a register

.data
  # Defines ASCII strings for printing
  label1:	.asciiz "\nHello, have a nice day.\n"
  name: .asciiz "Marcus Belcastro\n"
  class: .asciiz "This class is Computer Organisation\n"

.text
  li $v0, 4              # Loads print_str (system call num 4)

  la $a0, label1         # Loads the existing "hello world" as an argument
  syscall                # Print

  la $a0, name           # Loads my name string
  syscall                # Print

  la $a0, class          # Loads my class string
  syscall                # Print

  addu	$ra, $0, $s7     # Restore the return address
  jr $ra                 # Return to the main program
  add $0, $0, $0         # Do nothing
