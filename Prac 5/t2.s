# Registers used:
#
# $s0 = N
# $s1 = Running sum for array Q
# $s7 = return address from main
# $t0 = loop counter
# $t1 = index
# $t2 = index * 4
# $t3 = user value


.text
main:
	move $s7, $ra		# Save return address


getN:
	li $v0, 4		# Load the print_str syscall
	la $a0, promptN		# Load the prompt for the value of N
	syscall

	li $v0, 5		# Load the read_int syscall
	syscall

	bgt $v0, 9, getN	# If N greater than 9, try again
	blt $v0, 1, getN	# If N less than 1, try again

	move $s0, $v0		# Save the value of N

	li $v0, 4		# Load the print_str syscall
	la $a0, promptInput	# Load the prompt for the values of the arrays
	syscall


writeloop:
	li $v0, 5		# Load the read_int syscall
	syscall

	move $t3, $v0		# Save the value from the user temporarily

	add $s1, $s1, $t3	# Sum = Sum + new value

	sw $t3, P($t2)		# Store the input to P[index * 4]
	sw $s1, Q($t2)		# Store the sum to Q[index * 4]

	addi $t1, $t1, 1	# Index++
	mul $t2, $t1, 4		# index * 4

	blt $t1, $s0, writeloop # While index < N, keep asking for values


	move $t1, $0		# Clear the loop counter

	li $v0, 4		# Load the print_str syscall
	la $a0, promptOut	# Load the prompt for the output of the array
	syscall


readloop:
	mul $t2, $t1, 4		# index * 4

	li $v0, 4		# Load the print_str syscall
	la $a0, promptP		# Load the prompt for the array P
	syscall

	li $v0, 1		# Load the print_int syscall
	lw $a0, P($t2)		# Load the array value of P[index * 4]
	syscall

	li $v0, 4		# Load the print_str syscall
	la $a0, promptQ		# Load the prompt for the array Q
	syscall

	li $v0, 1		# Load the print_int syscall
	lw $a0, Q($t2)		# Load the array value of Q[index * 4]
	syscall

	addi $t1, $t1, 1	# Index++

	blt $t1, $s0, readloop	# While index < N, keep iterating through array


	# Return
	move $ra, $s7
	jr $ra
	nop


.data
.globl main
	P: .word 0, 1, 2, 3, 4, 5, 6, 7, 8
	Q: .word 0, 1, 2, 3, 4, 5, 6, 7, 8
	promptN: .asciiz "Type a length for the arrays: "
	promptInput: .asciiz "Type integers to put in the arrays:\n"
	promptOut: .asciiz "\nYour integers are:"
	promptP: .asciiz "\nP: "
	promptQ: .asciiz "\nQ: "
