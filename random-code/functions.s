.text
.globl main

main:
  la $a0, prompt1         #load the first prompt

  #prepare for first function call
  subu $sp, $sp, 32
  sw $ra, 20($sp)         #store return address
  sw $fp, 16($sp)         #store frame pointer
  addiu $fp, $sp, 28
  jal print_string        #call the print funtion

  #exit program
  li $v0, 10
  syscall


print_string:
  li $v0, 4
  syscall

  lw $ra, 20($sp)         #load the return address
  lw $fp, 16($sp)         #load the frame pointer
  addiu


print_int:
  li $v0, 1
  syscall



.data
newline: .asciiz "\n"

prompt1: .asciiz "First number: "
prompt2: .asciiz "Second number: "
prompt3: .asciiz "num1 + num2: "
