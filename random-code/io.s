.text
.globl main

main:
  #print prompt
  li $v0, 4         #load the print_string call
  la $a0, prompt1   #load the string to print
  syscall           #print it

  #Get user input
  li $v0, 5         #load the read_int call
  syscall           #read integer
  move $t1, $v0     #store integer in t0

  #print prompt
  li $v0, 4
  la $a0, prompt2
  syscall

  #Print the integer
  li $v0, 1         #load the print_int call
  move $a0, $t1     #load the collected integer to a0
  syscall           #print the integer

  li $v0, 10        #load the exit call
  syscall           #exit program


.data
prompt1: .asciiz "Type an integer: "
prompt2: .asciiz "Your integer: "
