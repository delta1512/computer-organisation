# Used registers:
#
# s7 - return address
# s6 - const 8
# s0 - packed word with 4 chars
# s1 - bitmask
# t0 - for loop counter getchars
# t1 - for loop counter showchars
# t2 - temporary shifting of characters back to the LSB
# t3 - calculation of a shamt

.text
.globl main

main:
	move $s7, $ra		# Save return address
	li $s6, 8		# Load a constant 8
	
	# Show prompt
	li $v0, 4
	la $a0, prompt
	syscall
	
	# Get 4 characters from the user and pack into one word
getchars:
	li $v0, 12		# Load the read_char syscall
	syscall
	sll $s0, $s0, 8		# Make room for the next word
	or $s0, $s0, $v0	# Load the char
	
	addi $t0, $t0, 1	# i++
	
	bne $t0, 4, getchars	# while i <= 4: getchars
	
	
	
	li $s1, 255		# Set up the mask
	
	# Show result prompt
	li $v0, 4
	la $a0, result
	syscall
	
	li $v0, 11		# Load print_char syscall
	
	
	
showchars:
	and $t2, $s0, $s1	# Get the next char from word string
	mul $t3, $t1, $s6	# shamt = i*8
	srlv $a0, $t2, $t3	# Shift char right by shamt
	syscall			# print it
	sll $s1, $s1, 8		# Shift the mask by 8 to get the next char
	
	addi $t1, $t1, 1	# i++
	
	bne $t1, 4, showchars	# while i <= 4: showchars
	
	
	# Exit main
	move $ra, $s7
	jr $ra
	nop


.data
	prompt: .asciiz "Type 4 characters: "
	result: .asciiz "\n\nYour chars reversed: "
	