.text
.globl main

main:
  li $v0, 4         #Load syscall code 4 for print string
  la $a0, hello     #Load string to print in a0
  syscall           #print

  li $v0, 10        #load the exit call
  syscall           #exit program


.data
hello: .asciiz "Hello world!"
