# Registers used:
#	s0 - maximum value
#	s1 - index i
#	s7 - saved return address
#	t0 - current array value at index
#	t1 - i * 4
#

.text
.globl main

main:
	move $s7, $ra		# Get the return address
	
	lw $t0, A ($zero)	# Load the first value into the comparison register
	li $s1, 1 		# Load the index for the array
	
next:	beq $t0, -1, done	# If not array end, search for max...

	blt $t0, $s0, ngt	# If new value is smaller than previous max, skip max setting
	move $s0, $t0		# Else make this value the new max

ngt:
	addi $s1, $s1, 1	# i++
	mul $t1, $s1, 4		# i * 4 in t1
	lw $t0, A ($t1)		# Loads the next element into the comparison register
	b next			# Loop

done:
	# print the result
	li $v0, 1
	move $a0, $s0
	syscall
	
	# Return
	move $ra, $s7
	jr $ra
	nop


.data
	# Assuming array terminates with -1
	# here are some test values:
	#A: .word 85, 24, 64, 39, 98, 52, 6, 22, 46, 13, -1 
	#A: .word 75, 71, 70, 51, 78, 49, 34, 76, 13, 21, -1
	A: .word 44, 5, 3, 76, 1, 26, 81, 79, 67, 78, -1