# 10-print program in MIPS
.globl main
main:
	li $v0, 4
	la $a0, greeting
	syscall
	
	# Get the first character
	la $a0, prompt0
	syscall
	
	li $v0, 12
	syscall
	
	move $s0, $v0
	
	li $v0, 4	#endl
	la $a0, endl
	syscall
	
	# Get the second character
	li $v0, 4
	la $a0, prompt1
	syscall
	
	li $v0, 12
	syscall
	
	move $s1, $v0
	
	li $v0, 4	#endl
	la $a0, endl
	syscall
	
	# Get the size
	li $v0, 4
	la $a0, promptnum
	syscall
	
	li $v0, 5
	syscall
	
	move $a2, $v0	#load the int as the 3rd argument
	move $a0, $s0	#load the first char as first arg
	move $a1, $s1	#load the second char as second arg
	
	subi $sp, $sp, 12
	sw $ra, ($sp)
	sw $s0, 4($sp)
	sw $s1, 8($sp)
	
	jal tenprint
	
	lw $s1, 8($sp)
	lw $s0, 4($sp)
	lw $ra, ($sp)
	addi $sp, $sp, 12
	
	li $v0, 4	#endl
	la $a0, endl
	syscall
	
	la $a0, printanother
	syscall
	
	li $v0, 12
	syscall
	
	move $t0, $v0
	
	li $v0, 4	#endl
	la $a0, endl
	syscall
	
	beq $t0, 0x59, main
	beq $t0, 0x79, main
	
	jr $ra
	nop
	
	
	

tenprint:
	subi $sp, $sp, 12
	sw $t0, ($sp)
	sw $t1, 4($sp)
	sw $ra, 8($sp)
	
	move $s0, $a0
	move $s1, $a1
	move $t0, $a2	#x = n
	move $t1, $a2	#y = n
	
	
	loopcols:
	# Restore ra from jal below
	lw $ra, 8($sp)
	jal getrand10
	
	beq $v0, $0, got0
	
		got1:
		move $a0, $s1
		j print
		
		got0:
		move $a0, $s0
	
		print:
		li $v0, 11
		syscall
	
	subi $t0, $t0, 1 #x--
	bne $t0, $0, loopcols
	move $t0, $a2 # x = n
	
	
	looprows:
	li $v0, 4	#endl
	la $a0, endl
	syscall
	
	subi $t1, $t1, 1 #y--
	bne $t1, $0, loopcols
	
	
	lw $ra, 8($sp)
	lw $t1, 4($sp)
	lw $t0, ($sp)
	addi $sp, $sp, 12
	
	jr $ra
	



getrand10:
	li $v0, 43
	syscall
	l.s $f1, probability
	c.lt.s $f0, $f1
	bc1t return1
	li $v0, 0
	jr $ra
	
	return1:
	li $v0, 1
	jr $ra
	


.data
	greeting: .asciiz "Welcome to Delta's magical 10print program!\n"
	prompt0: .asciiz "Choose a character: "
	prompt1: .asciiz "Choose another character: "
	promptnum: .asciiz "How big do you want it? "
	printanother: .asciiz "Again (you can only press y/Y)? "
	endl: .asciiz "\n"
	probability: .float 0.5