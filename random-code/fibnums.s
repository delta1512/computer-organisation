.text
.globl main

main:
	addu $s7, $zero, $ra 	# Save the return address
	
	li $v0, 1		# Load the print_int
	# Start with the first two fib nums
	li $s0, 1
	li $s1, 1
	
	# Print 1 twice
	li $v0, 1		# Load the print_int
	move $a0, $s0
	syscall
	# Print a newline
	li $v0, 4		# Load the print_str
	la $a0, endl
	syscall
	li $v0, 1		# Load the print_int
	move $a0, $s1
	syscall
	# Print a newline
	li $v0, 4		# Load the print_str
	la $a0, endl
	syscall
	
	# Initialise the loop counter
	li $s3, 10
fib:	
	beq $s3, $zero, done
	# Calculate new fib
	add $t0, $s0, $s1
	# Remove the old number and move new num in
	move $s0, $s1
	move $s1, $t0
	
	# Print the new number
	li $v0, 1
	move $a0, $s1
	syscall
	# Print a newline
	li $v0, 4
	la $a0, endl
	syscall
	
	# Decrement the loop counter
	subi $s3, $s3, 1
	# Loop
	b fib # (goes back to the start of the loop)
	
done:
	addu $ra, $zero, $s7    # Restore return address
	jr $ra			# return 
	nop


.data
	endl: .asciiz "\n"