.text
.globl main
main:
    li $v0, 33
    li $a1, 1000 # Set midi duration
    li $a2, 81 # Set instrument
    li $a3, 127 # Set colume to the max
    
    li $s0, 0 # counter
    loop:
    la $a0, ($s0)
    syscall
    addi $s0,$s0, 1
    bgt $s0, 127, main
    b loop