.globl main
main:
	# Load all of the constants
	l.d $f2, one		# Load T
	l.d $f4, five		# Load N
	# t2 starts at 0 and is stored in $f6
	l.d $f8, step		# Load the step for t2 (0.1)
	
amdahl:
	# The following calculates s = 1 / ( (t2/5) + (1-t2) )
	# Result will be in $f10
	sub.d $f30, $f2, $f6	# 1 - t2
	div.d $f28, $f6, $f4	# t2 / 5
	add.d $f10, $f30, $f28	# (t2/5) + (1-t2)
	div.d $f10, $f2, $f10	# 1 / ( (t2/5) + (1-t2) )
	
	li $v0, 4		# Load the print_str code
	la $a0, calc1		# Load the prompt as an argument
	syscall
	
	li $v0, 3		# Load the print_double code
	mov.d $f12, $f6		# Get t2 as the argument
	syscall	
	
	li $v0, 4		# Load the print_str code
	la $a0, calc2		# Load the prompt as an argument
	syscall
	
	li $v0, 3		# Load the print_double code
	mov.d $f12, $f10	# Get s as the argument
	syscall
	
	add.d $f6, $f6, $f8	# t2 += 0.1
	c.le.d $f6, $f2		# If t2 <= 1
	bc1t amdahl		# Keep looping
	
	
	# Return
	jr $ra
	nop
	

.data
	calc1: .asciiz "\nAt time percentage "
	calc2: .asciiz " the speedup is: "
	one: .double 1.0
	five: .double 5.0
	step: .double 0.1
	