.globl main
main:
	# Load some constants into the FPU
	l.d $f28, zero
	l.d $f30, twenty
	
	li $v0, 4		# Load the print_str code
	la $a0, promptT		# Load the prompt as an argument
	syscall
	
	li $v0, 7		# Load the read_double code
	syscall
	
	mov.d $f2, $f0		# Save T

getN:
	li $v0, 4		# Load the print_str code
	la $a0, promptN		# Load the prompt as an argument
	syscall
	
	li $v0, 7		# Load the read_double code
	syscall
	
	c.lt.d $f0, $f28	# If N < 0
	bc1t getNerr		# Print error and try again
	c.lt.d $f30, $f0	# If 20 < N
	bc1t getNerr		# Print error and try again
	
	mov.d $f4, $f0		# Save N

gett2:
	li $v0, 4		# Load the print_str code
	la $a0, promptt2	# Load the prompt as an argument
	syscall
	
	li $v0, 7		# Load the read_double code
	syscall
	
	c.lt.d $f2, $f0		# If T < t2
	bc1t gett2err		# Print error and try again
	
	mov.d $f6, $f0		# Save t2
	
	# The following calculates s = T / ( (t2/N) + (T-t2) )
	sub.d $f8, $f2, $f6	# T - t2
	div.d $f10, $f6, $f4	# t2 / N
	add.d $f12, $f8, $f10	# (t2/N) + (T-t2)
	div.d $f12, $f2, $f12	# T / ( (t2/N) + (T-t2) )
	
	li $v0, 4		# Load the print_str code
	la $a0, finalprompt	# Load the prompt as an argument
	syscall
	
	li $v0, 3		# Load the print_double code
	syscall	
	
	
	# Return
	jr $ra
	nop


getNerr:
	li $v0, 4		# Load the print_str code
	la $a0, promptNerr	# Load the error as an argument
	syscall
	
	b getN
	
gett2err:
	li $v0, 4		# Load the print_str code
	la $a0, promptt2err	# Load the error as an argument
	syscall
	
	b gett2


.data
	promptT: .asciiz "\nInput value T: "
	promptN: .asciiz "\nInput value N: "
	promptt2: .asciiz "\nInput value t2: "
	finalprompt: .asciiz "\nThe speedup is: "
	promptNerr: .asciiz "\nN must be between 0 and 20"
	promptt2err: .asciiz "\nt2 must be less than T"
	zero: .double 0.0
	twenty: .double 20.0