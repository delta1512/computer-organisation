# start of the main program
# this program can be used as a starting point to simulate logic
# circuits used to build ALU. It only implements one logic function.
# Author: Derek Bem, 2008

  .text
  .globl main
main:                        # main has to be a global label
    addu $s7, $0, $ra        # save the return address in a global register

#------------------------------------ getting a
    .data
    .globl  message1
message1:  .asciiz "\n\nInput 0 or 1 for a: "  #string to print

    .text
geta:
    li   $v0, 4              # print_string (system call 4)
    la   $a0, message1       # takes the address of string as an argument 
  syscall
  
    li   $v0, 5              # read_int (system call 5) 
    syscall          
   
   la $a1, geta		     # Load the address of this section of code incase we need to jump back
   beq $v0, -1, exit	     # Exit if -1
   blt $v0, $0, inputError   # If < 0, raise error
   bgt $v0, 1, inputError    # If > 1, raise error
   # else continue
   add  $s3, $0, $v0        # move to $s3
  
#------------------------------------ getting b
    .data
    .globl  message2
message2:  .asciiz "Input 0 or 1 for b: "  #string to print

    .text
getb:
    li   $v0, 4              # print_str (system call 4)
    la   $a0, message2       # takes the address of string as an argument 
    syscall
  
    li   $v0, 5              # read_int (system call 5)
    syscall          
    
   la $a1, getb		     # Load the address of this section of code incase we need to jump back
   beq $v0, -1, exit	     # Exit if -1
   blt $v0, $0, inputError   # If < 0, raise error
   bgt $v0, 1, inputError    # If > 1, raise error
   # else continue
   add  $s4, $0, $v0        # move to $s4

#------------------------------------ getting carryIn
    .data
message5:  .asciiz "Input 0 or 1 for carry in: "  #string to print

    .text
carryIn:
    li   $v0, 4              # print_str (system call 4)
    la   $a0, message5       # takes the address of string as an argument 
    syscall
  
    li   $v0, 5              # read_int (system call 5)
    syscall          
    
   la $a1, carryIn	     # Load the address of this section of code incase we need to jump back
   beq $v0, -1, exit	     # Exit if -1
   blt $v0, $0, inputError   # If < 0, raise error
   bgt $v0, 1, inputError    # If > 1, raise error
   # else continue
   add  $s5, $0, $v0        # move to $s5

#----------------------------------- calculating carry out
    and $t0, $s5, $s3	# cin * a
    and $t1, $s5, $s4	# cin * b
    and $t2, $s4, $s3	# a * b
    or $t0, $t0, $t1	# (cin * a) + (cin * b)
    or $t0, $t0, $t2	# (cin * a) + (cin * b) + (a * b)
#----------------------------------- printing a AND b on the console
    .data
message3:  .asciiz "           carry out: "  # string to print
message4:  .asciiz "\n---------------------" # next string to print

    .text
    li   $v0, 4              # print_str (system call 4)
    la   $a0, message3       # takes the address of string as an argument 
    syscall

    li   $v0, 1              # print_int (system call 1)
    add  $a0, $0, $t0        # put value to print in $a0
    syscall  

    li   $v0, 4              # print_str (system call 4)
    la   $a0, message4       # takes the address of string as an argument 
    syscall

    j  main                  # back to calculating  

#----------------------------------- usual stuff at the end of the main
exit:
    addu $ra, $0, $s7        # restore the return address
    jr   $ra                 # return to the main program
    
 
inputError:
	# Print error message
	li $v0, 4
	la $a0, errMsg
	syscall
	
	# Jump back to the relevant label
	jr $a1
	
.data
	errMsg: .asciiz "Please enter either 1 or 0 (-1 to exit)\n"
