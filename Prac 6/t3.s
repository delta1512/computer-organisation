.text
.globl main

main:
	li $v0, 4		# Load the print_str syscall code
	la $a0, promptn		# Load the prompt for n
	syscall
	
	li $v0, 5		# Load the read_int syscall code
	syscall
	
	blt $v0, 2, error	# IF n < 2 try again
	bgt $v0, 45, error	# IF n > 45 try again
	
	move $t0, $v0		# Save n to a register
	move $a0, $t0		# Set up argument for fib number function
	
	
	# Push the return address to the stack
	subi $sp, $sp, 8	# Make room for 2 registers
	sw $ra, ($sp)		# Save the return address
	sw $t0, 4($sp)		# Save the user's n
	
	li $a1, 1		# load arg1 = 1
	
	jal fib
	
	# Pop the return address from the stack
	lw $t0, 4($sp)		# Load the user's n
	lw $ra, ($sp)		# Load the last data item
	addi $sp, $sp, 8	# Deallocate the space on the stack
	
	
	move $s1, $v0		# Save the return value from the fib function to a register
	
	li $v0, 4		# Load the print_str syscall code
	la $a0, promptout1	# Load the prompt for output
	syscall
	
	li $v0, 1		# Load the print_int syscall code
	move $a0, $t0		# Move the user's n to the argument for printing
	syscall
	
	li $v0, 4		# Load the print_str syscall code
	la $a0, promptout2	# Load the prompt for output
	syscall
	
	li $v0, 1		# Load the print_int syscall code
	move $a0, $s1		# Move the result to the argument for printing
	syscall
	
	
	jr $ra			# return 
	nop



# int fib(int term, int val1, int val2) {
# 	if(term == 0) {
#		return prev;
#	}
# 	return fib(--term, val1+val2, val1);
# }

fib:	
	# Push registers to the stack
	subi $sp, $sp, 4	# Make space for 1 value
	sw $ra, ($sp)		# Save the return address
	
	
	beq $a0, $0, done	# If term is 0, return
	
	subi $a0, $a0, 1	# Term - 1
	add $t0, $a1, $a2	# val1+val2
	move $a2, $a1		# val2 = val1
	move $a1, $t0		# val1 = val1+val2
	
	jal fib
	
done:
	move $v0, $a2		# Set up return value
	
	
	# Pop registers from stack
	lw $ra, ($sp)		# Load the return address
	addi $sp, $sp, 4	# Deallocate space from the stack
	
	jr $ra			# Return


error:
	li $v0, 4		# Load the print_str syscall code
	la $a0, errormsg	# Load the error message
	syscall
	
	b main			# Try for user input again


.data
	promptn: .asciiz "Choose a number between 2 and 45: "
	promptout1: .asciiz "Fibonacci number "
	promptout2: .asciiz " is: "
	errormsg: .asciiz "\nIncorrect input\n"
