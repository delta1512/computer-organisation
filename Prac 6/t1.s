.text
.globl main

main:
	li $v0, 4		# Load the print_str syscall code
	la $a0, promptn		# Load the prompt for n
	syscall
	
	li $v0, 5		# Load the read_int syscall code
	syscall
	
	blt $v0, 2, error	# IF n < 2 try again
	bgt $v0, 45, error	# IF n > 45 try again
	
	move $s0, $v0		# Save n to a register
	move $a0, $s0		# Set up argument for fib number function
	
	
	# Push the return address to the stack
	subi $sp, $sp, 4	# Make room for 1 register
	sw $ra, ($sp)		# Save the return address
	
	jal fib
	
	# Pop the return address from the stack
	lw $ra, ($sp)		# Load the last data item
	addi $sp, $sp, 4	# Deallocate the space on the stack
	
	
	move $s1, $v0		# Save the return value from the fib function to a register
	
	li $v0, 4		# Load the print_str syscall code
	la $a0, promptout1	# Load the prompt for output
	syscall
	
	li $v0, 1		# Load the print_int syscall code
	move $a0, $s0		# Move the user's n to the argument for printing
	syscall
	
	li $v0, 4		# Load the print_str syscall code
	la $a0, promptout2	# Load the prompt for output
	syscall
	
	li $v0, 1		# Load the print_int syscall code
	move $a0, $s1		# Move the result to the argument for printing
	syscall
	
	
	jr $ra			# return 
	nop


fib:	
	# Load F0 and F1
	move $t0, $0
	li $t1, 1
	
	# Initialise the loop counter
	li $t3, 1
	
fibloop:
	add $t2, $t0, $t1	# Calculate new Fn
	move $t0, $t1		# Move Fn-1 back
	move $t1, $t2		# Move Fn back
	
	addi $t3, $t3, 1	# i++
	
	blt $t3, $a0, fibloop	# Exit if user number found
	
	move $v0, $t2		# Set up return value
	
	jr $ra			# Return
	

error:
	li $v0, 4		# Load the print_str syscall code
	la $a0, errormsg	# Load the error message
	syscall
	
	b main			# Try for user input again


.data
	promptn: .asciiz "Choose a number between 2 and 45: "
	promptout1: .asciiz "Fibonacci number "
	promptout2: .asciiz " is: "
	errormsg: .asciiz "\nIncorrect input\n"
