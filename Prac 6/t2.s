.text
.globl main

main:
	li $v0, 4		# Load the print_str syscall code
	la $a0, promptn		# Load the prompt for n
	syscall
	
	li $v0, 5		# Load the read_int syscall code
	syscall
	
	blt $v0, 2, error	# IF n < 2 try again
	bgt $v0, 50, error	# IF n > 50 try again
	
	move $s0, $v0		# Save n to a register
	move $a0, $s0		# Set up argument for fib number function
	
	
	# Push the return address to the stack
	subi $sp, $sp, 4	# Make room for 1 register
	sw $ra, ($sp)		# Save the return address
	
	jal fib
	
	# Pop the return address from the stack
	lw $ra, ($sp)		# Load the last data item
	addi $sp, $sp, 4	# Deallocate the space on the stack
	
	
	li $v0, 4		# Load the print_str syscall code
	la $a0, promptout1	# Load the prompt for output
	syscall
	
	li $v0, 1		# Load the print_int syscall code
	move $a0, $s0		# Move the user's n to the argument for printing
	syscall
	
	li $v0, 4		# Load the print_str syscall code
	la $a0, promptout2	# Load the prompt for output
	syscall
	
	li $v0, 3		# Load the print_double syscall code
	syscall
	
	
	jr $ra			# return 
	nop


fib:	
	# Load F0 and F1
	l.d $f0, double0
	l.d $f2, double1
	
	# Initialise the loop counter
	li $t3, 1
	
fibloop:
	add.d $f4, $f0, $f2	# Calculate new Fn
	mov.d $f0, $f2		# Move Fn-1 back
	mov.d $f2, $f4		# Move Fn back
	
	addi $t3, $t3, 1	# i++
	
	blt $t3, $a0, fibloop	# Exit if user number found
	
	mov.d $f12, $f4		# Set up final value
	
	jr $ra			# Return
	

error:
	li $v0, 4		# Load the print_str syscall code
	la $a0, errormsg	# Load the error message
	syscall
	
	b main			# Try for user input again


.data
	promptn: .asciiz "Choose a number between 2 and 50: "
	promptout1: .asciiz "Fibonacci number "
	promptout2: .asciiz " is: "
	errormsg: .asciiz "\nIncorrect input\n"
	double0: .double 0
	double1: .double 1
