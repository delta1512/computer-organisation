.globl main

main:
	li $s0, 7		# s0 holds the buffer size
	li $s1, 69		# s1 holds the terminating character 'E'
	li $s2, 0xffff  	# s2 holds the memory address of the mapped IO
	sll $s2, $s2, 16	# Shift it left to get the correct address 0xffff0000
	
	li $t0, 1		# Load a bit mask in t0
	
	li $v0, 4		# Load the print_str code
	la $a0, promptread	# Load the read prompt
	syscall
		
readloop:
	lw $t1, ($s2)		# Get the control info for keyboard device 
	and $t1, $t1, $t0	# Get the ready bit
	beq $t1, $0, readloop	# If not ready, keep checking
	
	lb $t1, 4($s2)		# Load the written character into t1
	beq $t1, $s1, write     # If terminating character encountered, exit loop
	
	sb $t1, buff($t3)	# Store the character in the correct index in the buffer
	addi $t3, $t3, 1	# i++
	
	bne $t3, $s0, readloop  # If buffer size not reached, then keep looping
		
write:
	li $v0, 4		# Load the print_str code
	la $a0, promptwrite	# Load the write prompt
	syscall
	
writeloop:
	lw $t1, 8($s2)		# Get the control info for keyboard device 
	and $t1, $t1, $t0	# Get the ready bit
	beq $t1, $0, writeloop	# If not ready, keep checking
	
	lb $t1, buff($t4)	# Read the next byte of the buffer
	sw $t1, 12($s2)		# Store the character in the terminal output MMIO address
	
	addi $t4, $t4, 1	# i++
	
	blt $t4, $t3, writeloop	# If there are still characters yet to be printed, continue
	
	li $v0, 4		# Load the print_str code
	la $a0, done		# Load the termination message
	syscall
		
	
	# Return
	jr $ra
	nop
	


.data
	buff: .space 7
	.align 2
	promptread: .asciiz "\nType 7 characters (E to stop) "
	promptwrite: .asciiz "\nSee what you typed "
	done: .asciiz "\nProgram terminated..."