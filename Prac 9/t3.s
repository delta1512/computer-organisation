.globl main

main:
	li $s0, 7		# s0 holds the buffer size
	li $s1, 69		# s1 holds the terminating character 'E'
	li $s2, 0xffff  	# s2 holds the memory address of the mapped IO
	sll $s2, $s2, 16	# Shift it left to get the correct address 0xffff0000
	
	li $t0, 1		# Load a bit mask in t0
	
	li $v0, 4		# Load the print_str code
	la $a0, promptread	# Load the read prompt
	syscall
	
read:
	# Clear all the registers
	move $t1, $0
	move $t3, $0
	move $t4, $0
		
readloop:
	lw $t1, ($s2)		# Get the control info for keyboard device 
	and $t1, $t1, $t0	# Get the ready bit
	beq $t1, $0, readloop	# If not ready, keep checking
	
	lb $t1, 4($s2)		# Load the written character into t1
	beq $t1, $s1, write     # If terminating character encountered, exit loop
	
	sb $t1, buff($t3)	# Store the character in the correct index in the buffer
	addi $t3, $t3, 1	# i++
	addi $s3, $s3, 1	# counter++
	
	bne $t3, $s0, readloop  # If buffer size not reached, then keep looping
		
write:
	move $s7, $t1		# Save the last character that was used
	
writeloop:
	lw $t1, 8($s2)		# Get the control info for keyboard device 
	and $t1, $t1, $t0	# Get the ready bit
	beq $t1, $0, writeloop	# If not ready, keep checking
	
	lb $t1, buff($t4)	# Read the next byte of the buffer
	sw $t1, 12($s2)		# Store the character in the terminal output MMIO address
	
	addi $t4, $t4, 1	# i++
	
	blt $t4, $t3, writeloop	# If there are still characters yet to be printed, continue
	
	bne $s7, $s1, read	# If the terminating character was not the last character, keep going
	
	li $v0, 1		# Load the  print_int code
	move $a0, $s3		# Load the count as an argument
	syscall	
	
	li $v0, 4		# Load the print_str code
	la $a0, countermsg	# Load the counter message
	syscall
	
	li $v0, 4		# Load the print_str code
	la $a0, done		# Load the termination message
	syscall
		
	
	# Return
	jr $ra
	nop
	


.data
	buff: .space 7
	.align 2
	promptread: .asciiz "\nType characters (E to stop)\n"
	countermsg: .asciiz " characters read and written"
	done: .asciiz "\nProgram terminated..."